//
//  MovieDetailGridCell.m
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import "MovieDetailGridCell.h"

@implementation MovieDetailGridCell

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.posterImage.image = [UIImage imageNamed:@"ph_image_small.png"];
    self.posterImage.contentMode = UIViewContentModeCenter;
}

@end
