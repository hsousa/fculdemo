//
//  MovieDetailGridCell.h
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieDetailGridCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *posterImage;

@end
