//
//  MovieGridCell.h
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieGridCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *movieImage;
@property (strong, nonatomic) IBOutlet UILabel *movieTitle;

@end
