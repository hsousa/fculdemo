//
//  MovieGridCell.m
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import "MovieGridCell.h"

@implementation MovieGridCell

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.movieImage.image = [UIImage imageNamed:@"ph_image_small.png"];
    self.movieImage.contentMode = UIViewContentModeCenter;
    self.movieTitle.text = nil;
}

@end
