//
//  AppDelegate.h
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
