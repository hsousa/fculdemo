//
//  MovieDetailViewController.h
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieDetailViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *posterGrid;

@property (nonatomic,strong) NSArray *movies;
@property (nonatomic,readwrite) NSInteger selectedMovieIndex;

@end
