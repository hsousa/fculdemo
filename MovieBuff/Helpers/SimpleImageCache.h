//
//  SimpleImageCache.h
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleImageCache : NSObject

+ (instancetype)sharedSimpleImageCache;

- (void)addImage:(UIImage*)image key:(NSString*)key;
- (UIImage*)imageForKey:(NSString*)key;
- (void)clearCache;

@end
