//
//  MovieManager.h
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieManager : NSObject

- (void)fetchPopularMoviesForYear:(NSUInteger)year includeAdult:(BOOL)adult completionHandler:(void (^)(NSArray* movies))handler;

@end
