//
//  MovieManager.m
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import "MovieManager.h"

static NSString *kMovieURL = @"https://api.themoviedb.org/3/discover/movie?api_key=ea644a530ad78ae928787551fdee1f16&include_adult=%@&primary_release_year=%d&sort_by=popularity.desc";

@implementation MovieManager

- (void)fetchPopularMoviesForYear:(NSUInteger)year
                     includeAdult:(BOOL)adult
                completionHandler:(void (^)(NSArray* movies))handler {
    NSString *url = [NSString stringWithFormat:kMovieURL, adult ? @"true" : @"false", year];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithURL:[NSURL URLWithString:url] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSError *jsonParseError = nil;
        NSDictionary *moviesJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &jsonParseError];
        
        if (!jsonParseError) {
            NSArray *movies = (NSArray*)moviesJSON[@"results"];
            if (handler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(movies);
                });
            }
        }
    }];
    [task resume];
}

@end
