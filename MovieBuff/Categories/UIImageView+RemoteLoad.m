//
//  UIImageView+RemoteLoad.m
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import "UIImageView+RemoteLoad.h"
#import "SimpleImageCache.h"

@implementation UIImageView (RemoteLoad)

#pragma mark - Public methods

- (void)setRemoteImageFromURL:(NSString*) url {
    UIImage *image = [[SimpleImageCache sharedSimpleImageCache] imageForKey:url];
    if (image) {
        self.image = image;
        self.contentMode = UIViewContentModeScaleAspectFill;
    } else {
        [self loadRemoteImageFromURL:url];
    }
}

#pragma mark - Private methods

- (void)loadRemoteImageFromURL:(NSString*) url {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:[NSURL URLWithString:url] completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        if (!error && location) {
            NSData *data = [NSData dataWithContentsOfFile:location.path];
            UIImage *image = [UIImage imageWithData:data];
            [[SimpleImageCache sharedSimpleImageCache] addImage:image key:url];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.image = image;
                self.contentMode = UIViewContentModeScaleAspectFill;
            });
        }
        
    }];
    [task resume];
}
@end
