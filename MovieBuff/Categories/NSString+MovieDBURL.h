//
//  NSString+MovieDBURL.h
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MovieDBURL)

- (NSString*)fullPosterURL;

@end
