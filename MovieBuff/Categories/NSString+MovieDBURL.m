//
//  NSString+MovieDBURL.m
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import "NSString+MovieDBURL.h"

@implementation NSString (MovieDBURL)

- (NSString*)fullPosterURL {
    return [NSString stringWithFormat:@"http://image.tmdb.org/t/p/w342/%@", self];
}

@end
