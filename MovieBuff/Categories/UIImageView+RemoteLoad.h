//
//  UIImageView+RemoteLoad.h
//  MovieBuff
//
//  Copyright (c) 2014 MovieBuff. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (RemoteLoad)

- (void)setRemoteImageFromURL:(NSString*)url;

@end
